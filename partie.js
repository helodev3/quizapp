import React from 'react';
import { Text, View ,Image,TouchableOpacity, Alert,AsyncStorage} from 'react-native';
import {Button,Icon} from "native-base"
import Question from "./Components/question"
import Response from "./Components/reponse"
import questionnaire from "./questionnaire"

export default class Partie extends React.Component{
    constructor(props){
        super(props)
        this.state={
            quest:questionnaire,
            ordre:0,
            etatA:0,
            etatB:0,
            etatC:0,
            etatD:0,
            score:0
        }
    }
    _pressed=(choisi)=>{
        
        const ordre=this.state.ordre
        const taille=this.state.quest.length-1
        sc=this.state.score
        if(ordre<taille && choisi===this.state.quest[this.state.ordre].bonneReponse){
            
            this.setState({
                etatA:0,
            etatB:0,
            etatC:0,
            etatD:0,

                ordre:ordre+1,
                score:sc+50
            })

        }
        else if (choisi!=this.state.quest[this.state.ordre].bonneReponse){
            if(sc<=25){
                Alert.alert("Fin de partie, Vous avez perdu");
                this.props.navigation.goBack()
                return
            }
            switch (choisi){
                case "A":
                    this.setState({etatA:1,score:sc-25})
                    break
                case "B":
                    this.setState({etatB:1,score:sc-25})
                    break
                case "C":
                    this.setState({etatC:1,score:sc-25})
                    break
                case "D":
                    this.setState({etatD:1,score:sc-25})
                    break
            }

            
        }
        else if (ordre==taille){
            const mScore= AsyncStorage.getItem("mScore")
            
            
            Alert.alert("Felicitations, votre gain est de "+sc+"$");
            if(mScore<sc){
                AsyncStorage.setItem("mScore",sc.toString())
            }
            this.props.navigation.goBack()
            return

        }
        
    }
    render(){
        return(
            <View style={{flex:1,justifyContent:"flex-start",
            alignItems:"center",backgroundColor:"blue"}}>
                <View style={{flexDirection:"row",height:50,width:"100%",marginTop:40,justifyContent:"space-between"}}>
                    <View style={{backgroundColor:"orange",height:50,justifyContent:"center",alignItems:"center",borderRadius:10,marginHorizontal:10}}>
                        <Text style={{fontWeight:"bold",fontSize:25,textAlign:"center"}}>{this.state.score.toString()+"$"}</Text>
                    </View>
                    <View style={{height:50,flex:1,flexDirection:"row",justifyContent:"flex-end",alignItems:"center",marginEnd:10}}>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="md-contacts" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="md-call" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="compass" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    </View>
                </View>
                <Question style={{marginTop:0}} question={this.state.quest[this.state.ordre].question} />
                <Response ordre="A :" etatA={this.state.etatA} reponse={this.state.quest[this.state.ordre].A} vrai={this.state.quest[this.state.ordre].bonneReponse} aLappui={()=>{this._pressed("A")}}/>
                <Response ordre="B :" etatB={this.state.etatB}  reponse={this.state.quest[this.state.ordre].B}   reponse={this.state.quest[this.state.ordre].B}  aLappui={()=>{this._pressed("B")}} vrai={this.state.quest[this.state.ordre].bonneReponse} />
                <Response ordre="C :"  etatC={this.state.etatC}  reponse={this.state.quest[this.state.ordre].C}   aLappui={()=>{this._pressed("C")}} vrai={this.state.quest[this.state.ordre].bonneReponse} />
                <Response ordre="D :"  etatD={this.state.etatD}  reponse={this.state.quest[this.state.ordre].D}   aLappui={()=>{this._pressed("D")}} vrai={this.state.quest[this.state.ordre].bonneReponse} />
                

            </View>
        )
    }
}


















/*
<View style={{flexDirection:"row",height:50,width:"100%",marginTop:40,justifyContent:"space-between"}}>
                    <View style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,marginHorizontal:10}}>
                        <Text style={{fontWeight:"bold",fontSize:25,textAlign:"center"}}>0$</Text>
                    </View>
                    <View style={{height:50,flex:1,flexDirection:"row",justifyContent:"flex-end",alignItems:"center",marginEnd:10}}>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="md-contacts" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="md-call" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor:"orange",height:50,width:50,justifyContent:"center",alignItems:"center",borderRadius:10,flexDirection:"row",marginHorizontal:5}}>
                        <Icon name="compass" style={{fontWeight:"bold",fontSize:35,textAlign:"center"}}/>
                        
                    </TouchableOpacity>
                    </View>
                </View>
                <Question style={{marginTop:0}} question="Premiere question" />
                <Response ordre="A :" reponse="Premiere reponse" />
                <Response ordre="B :" reponse="Deuxieme reponse" />
                <Response ordre="C :" reponse="Troisieme reponse" />
                <Response ordre="D :" reponse="Quatrieme reponse" />
                */
