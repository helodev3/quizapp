import React from "react"
import {View ,Text,TouchableOpacity }from "react-native"
import {Button }from "native-base"

export default class Response extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        const reponse=this.props.reponse
        const vrai = this.props.vrai
        
        switch(this.props.ordre){
            case "A :":
                return(this.props.etatA===1?<TouchableOpacity style={{backgroundColor:"red",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
                <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
                </View>
                <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
                </View>
                
                
            </TouchableOpacity>:<TouchableOpacity style={{backgroundColor:"orange",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
            <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
            </View>
            <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
            </View>
            
            
                </TouchableOpacity>)            
            case "B :":
                return(this.props.etatB===1?<TouchableOpacity style={{backgroundColor:"red",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
                <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
                </View>
                <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
                </View>
                
                
            </TouchableOpacity>:<TouchableOpacity style={{backgroundColor:"orange",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
            <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
            </View>
            <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
            </View>
            
            
                </TouchableOpacity>)
            case "C :":
                return(this.props.etatC===1?<TouchableOpacity style={{backgroundColor:"red",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
                <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
                </View>
                <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
                </View>
                
                
            </TouchableOpacity>:<TouchableOpacity style={{backgroundColor:"orange",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
            <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
            </View>
            <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
            </View>
            
            
                </TouchableOpacity>)
            case "D :":
                return(this.props.etatD===1?<TouchableOpacity style={{backgroundColor:"red",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
                <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
                </View>
                <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
                </View>
                
                
            </TouchableOpacity>:<TouchableOpacity style={{backgroundColor:"orange",width:"80%",flexDirection:"row",height:60,marginBottom:25}} onPress={this.props.aLappui}>
            
            <View style={{width:"20%",backgroundColor:"#0066ff",alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:25,color:"white",textAlign:"center"}}>{this.props.ordre}</Text>
            </View>
            <View style={{width:"80%",justifyContent:"center",alignItems:"center"}}>
                <Text style={{fontSize:25,color:"black",textAlign:"center"}}>{reponse}</Text>
            </View>
            
            
                </TouchableOpacity>)
        }

        
    }
}