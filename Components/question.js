import React from "react"
import {View ,Text} from "react-native"

export default class Question extends React.Component {

    render(){
        return(
        <View style={{backgroundColor:"#d4d4d4",height:200,width:"80%",justifyContent:"center",alignItems:"center",marginTop:50,marginBottom:50}}>
            <Text style={{fontSize:20}} >{this.props.question}</Text>
        </View>)
    }
}