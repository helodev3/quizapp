import React from 'react';
import { StyleSheet, Text, View ,Image,Alert,AsyncStorage} from 'react-native';
import {Button} from "native-base"
import {createAppContainer} from "react-navigation"
import {createStackNavigator} from "react-navigation-stack";
import Partie from "./partie"

class App extends React.Component{
  render(){
    const mScore= AsyncStorage.getItem("mScore").valeur
    if(mScore!=null){
      sc=mScore;
    }
    else{
      sc=0
    }
    return (
    <View style={styles.container}>
      <Image source={require("./assets/logo.png")} style={{marginVertical:100}}/>

      <View style={{width:"80%"}}>
        
        <Button full primary style={{marginBottom:15}} onPress={()=>{
          this.props.navigation.push("Partie")}}>
          <Text style={{fontSize:22,color:"white"}}>New Game</Text>
        </Button>
        <Button full primary style={{marginBottom:15}} onPress={()=>{Alert.alert(sc.toString())
        
        }}><Text style={{fontSize:22,color:"white"}}>High Score</Text></Button>
        <Button full primary style={{marginBottom:15}}><Text style={{fontSize:22,color:"white"}}>About</Text></Button>
        <Button full primary style={{marginBottom:15}}><Text style={{fontSize:22,color:"white"}}>Exit</Text></Button>

        
      </View>
      
    </View>
  );}
}
const navigator=createStackNavigator(
  {
  Home:App,
  Partie:Partie
},
  {
    initialRouteName:"Home",
    headerMode:"none"
  }


)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"orange",
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

export default createAppContainer(navigator)







